#!yab

#This program is a better Hello World
//We can make comments
REM in differnet ways

//open a window
window open 100,100 to 400,150, "HelloWorldWindow", "Example Program"

//make a view in the window
//the window itself is a kind of view, but DRAW SET does  not work on it
view 0,0 to 300,50, "HelloWorldView", "HelloWorldWindow"

//set the font - need the dejavu package for this example.
draw set "DejaVu Sans, Bold, 40", "HelloWorldView"

//draw the text
draw text 5, 40, "Hello, World!", "HelloWorldView"

//start an infinite loop that can only be broken if the
//user closes the program
while(instr(message$, "Quit") =0)
print "checking for message from Haiku..."

//this shows the processes
wend
print "exiting program"
//delete this when you understand it
exit